const serverless = require('serverless-http');
const express = require('express');
const app = express();
//https://www.npmjs.com/package/ml
const ml = require('ml');
const naivebayes = require('ml-naivebayes');
const dt = require('ml-cart');
const rf = require('ml-random-forest');
const csv = require('csvtojson');
const csvSocialNetworkFilePath = 'Social_Network_Ads.csv';
const csvPositionSalareiesFilePath = 'Position_Salaries.csv';
const PERCENTAGE_FOR_TRAINING = 0.8;

let csvData = [], // parsed Data
    X = [], // Input
    y = [], // Output
    X_test = [],
    y_test = [],
    X_train = [],
    y_train = [];

app.get('/knn', function (req, res) {
    csv().fromFile(csvSocialNetworkFilePath).subscribe(jsonObj => {
        csvData.push(jsonObj);
    }).on('done', () => {
        // To get data points from JSON Objects and split into training set and test set
        preProcessData();
        const model = new ml.KNN(X_train, y_train, { k: 1 });
        const predictions = model.predict(X_test);
        const accuracy = calculateAccuracy(predictions, y_test);
        res.send('Predictions: ' + JSON.stringify({
            accuracy: accuracy + '%'
        }));
    });
});

app.get('/naivebayes', function (req, res) {
    csv().fromFile(csvSocialNetworkFilePath).subscribe(jsonObj => {
        csvData.push(jsonObj);
    }).on('done', () => {
        // To get data points from JSON Objects and split into training set and test set
        preProcessData();
        const model = new naivebayes.GaussianNB();
        model.train(X_train, y_train);
        const predictions = model.predict(X_test);
        const accuracy = calculateAccuracy(predictions, y_test);
        res.send('Predictions: ' + JSON.stringify({
            accuracy: accuracy + '%'
        }));
    });
});

app.get('/decisionTree', function (req, res) {
    csv().fromFile(csvSocialNetworkFilePath).subscribe(jsonObj => {
        csvData.push(jsonObj);
    }).on('done', () => {
        // To get data points from JSON Objects and split into training set and test set
        preProcessData();
        const model = new dt.DecisionTreeClassifier({
            gainFunction: 'gini',
            maxDepth: 10,
            minNumSamples: 30
        });
        model.train(X_train, y_train);
        const predictions = model.predict(X_test);
        const accuracy = calculateAccuracy(predictions, y_test);
        res.send('Predictions: ' + JSON.stringify({
            accuracy: accuracy + '%'
        }));
    });
});

app.get('/randomForest', function (req, res) {
    csv().fromFile(csvSocialNetworkFilePath).subscribe(jsonObj => {
        csvData.push(jsonObj);
    }).on('done', () => {
        // To get data points from JSON Objects and split into training set and test set
        preProcessData();
        var model = new rf.RandomForestClassifier({
            seed: 3,
            nEstimators: 10
        });
        // // Train the classifier - we give him an xor
        model.train(X_train, y_train);
        let predictions = model.predict(X_test);

        let accuracy = calculateAccuracy(predictions, y_test);
        res.send('Predictions: ' + JSON.stringify({
            accuracy: accuracy + '%'
        }));
    });
});

app.get('/randomForestRegression', function (req, res) {
    csv().fromFile(csvPositionSalareiesFilePath).subscribe(jsonObj => {
        csvData.push(jsonObj);
    }).on('done', () => {
        // To get data points from JSON Objects and split into training set and test set
        preProcessRegressionData();
        const model = new dt.DecisionTreeRegression();
        model.train(X, y);
        const prediction = model.predict([[6.5]]);
        res.send('Predictions: ' + JSON.stringify({
            salary: Math.round(prediction)
        }));
    });
});

app.get('/polynomialRegression', function (req, res) {
    csv().fromFile(csvPositionSalareiesFilePath).subscribe(jsonObj => {
        csvData.push(jsonObj);
    }).on('done', () => {
        // To get data points from JSON Objects and split into training set and test set
        preProcessRegressionData();
        const model = new ml.PolynomialRegression(X, y, 4);
        // Train the classifier - we give him an xor
        model.train(X, y);
        const prediction = model.predict([[6.5]]);
        res.send('Predictions: ' + JSON.stringify({
            salary: Math.round(prediction)
        }));
    });
});

function calculateAccuracy(predictions, y_test) {
    return ml.ConfusionMatrix.fromLabels(y_test, predictions).getAccuracy() * 100;
}

function preProcessRegressionData() {
    csvData.forEach((row) => {
        X.push([f(row.Level)]);
        y.push(f(row.Salary));
    });
    splitTrainingTestSet(X, y);
}

function preProcessData() {
    csvData.forEach((row) => {
        X.push([f(row.Age), f(row.EstimatedSalary)]);
        y.push(f(row.Purchased));
    });
    splitTrainingTestSet(X, y);
}

function splitTrainingTestSet(X, y) {
    X_train = X.slice(0, X.length * PERCENTAGE_FOR_TRAINING);
    y_train = y.slice(0, y.length * PERCENTAGE_FOR_TRAINING);

    X_test = X.slice(X.length * PERCENTAGE_FOR_TRAINING, X.length);
    y_test = y.slice(y.length * PERCENTAGE_FOR_TRAINING, y.length);
}

function f(s) {
    return parseFloat(s);
}

module.exports.handler = serverless(app);